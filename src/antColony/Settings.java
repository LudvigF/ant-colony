package antColony;

public class Settings {

	//BOARD
	public static final int FACTOR = 5;
	public static int ANTS = 500;
	
	public static final int I = 1; 
	
	//ANT
	public static int FERMONEAMOUNT = 40*I;
	public static int PROBABILITY_SOFTNER = 5000*I;
	
	//TILE
	public static int MAX_FEREMONE = 10000*I;
	public static int MIN_FEREMONE = 00;
	public static int DECREASE_RATE = 1;
	
}
